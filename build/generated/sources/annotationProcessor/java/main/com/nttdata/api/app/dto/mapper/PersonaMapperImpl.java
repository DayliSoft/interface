package com.nttdata.api.app.dto.mapper;

import com.nttdata.api.app.dto.PersonaDTO;
import com.nttdata.api.app.vo.ClienteVO;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-07-10T15:37:36-0500",
    comments = "version: 1.4.1.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.4.1.jar, environment: Java 18.0.1.1 (Oracle Corporation)"
)
public class PersonaMapperImpl implements PersonaMapper {

    @Override
    public PersonaDTO toDto(ClienteVO clienteVO) {
        if ( clienteVO == null ) {
            return null;
        }

        PersonaDTO personaDTO = new PersonaDTO();

        personaDTO.setId( clienteVO.getId() );
        personaDTO.setNombre( clienteVO.getNombre() );
        personaDTO.setGenero( clienteVO.getGenero() );
        personaDTO.setEdad( clienteVO.getEdad() );
        personaDTO.setIdentificacion( clienteVO.getIdentificacion() );
        personaDTO.setDireccion( clienteVO.getDireccion() );
        personaDTO.setTelefono( clienteVO.getTelefono() );

        return personaDTO;
    }
}
