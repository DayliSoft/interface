package com.nttdata.api.app.dto.mapper;

import com.nttdata.api.app.dto.ClienteDTO;
import com.nttdata.api.app.dto.CuentaDTO;
import com.nttdata.api.app.vo.ClienteVO;
import com.nttdata.api.app.vo.CuentaVO;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-07-10T15:37:36-0500",
    comments = "version: 1.4.1.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.4.1.jar, environment: Java 18.0.1.1 (Oracle Corporation)"
)
public class CuentaMapperImpl implements CuentaMapper {

    @Override
    public CuentaDTO toDto(CuentaVO cuentaVO) {
        if ( cuentaVO == null ) {
            return null;
        }

        CuentaDTO cuentaDTO = new CuentaDTO();

        cuentaDTO.setId( cuentaVO.getId() );
        cuentaDTO.setNumeroCuenta( cuentaVO.getNumeroCuenta() );
        cuentaDTO.setTipoCuenta( cuentaVO.getTipoCuenta() );
        cuentaDTO.setSaldoInicial( cuentaVO.getSaldoInicial() );
        cuentaDTO.setEstado( cuentaVO.getEstado() );
        cuentaDTO.setCliente( clienteVOToClienteDTO( cuentaVO.getCliente() ) );
        cuentaDTO.setClienteId( cuentaVO.getClienteId() );

        return cuentaDTO;
    }

    @Override
    public CuentaVO toVo(CuentaDTO cuentaDTO) {
        if ( cuentaDTO == null ) {
            return null;
        }

        CuentaVO cuentaVO = new CuentaVO();

        cuentaVO.setId( cuentaDTO.getId() );
        cuentaVO.setNumeroCuenta( cuentaDTO.getNumeroCuenta() );
        cuentaVO.setTipoCuenta( cuentaDTO.getTipoCuenta() );
        cuentaVO.setSaldoInicial( cuentaDTO.getSaldoInicial() );
        cuentaVO.setEstado( cuentaDTO.getEstado() );
        cuentaVO.setClienteId( cuentaDTO.getClienteId() );
        cuentaVO.setCliente( clienteDTOToClienteVO( cuentaDTO.getCliente() ) );

        return cuentaVO;
    }

    protected ClienteDTO clienteVOToClienteDTO(ClienteVO clienteVO) {
        if ( clienteVO == null ) {
            return null;
        }

        ClienteDTO clienteDTO = new ClienteDTO();

        clienteDTO.setId( clienteVO.getId() );
        clienteDTO.setNombre( clienteVO.getNombre() );
        clienteDTO.setGenero( clienteVO.getGenero() );
        clienteDTO.setEdad( clienteVO.getEdad() );
        clienteDTO.setIdentificacion( clienteVO.getIdentificacion() );
        clienteDTO.setDireccion( clienteVO.getDireccion() );
        clienteDTO.setTelefono( clienteVO.getTelefono() );
        clienteDTO.setPassword( clienteVO.getPassword() );
        clienteDTO.setEstado( clienteVO.getEstado() );

        return clienteDTO;
    }

    protected ClienteVO clienteDTOToClienteVO(ClienteDTO clienteDTO) {
        if ( clienteDTO == null ) {
            return null;
        }

        ClienteVO clienteVO = new ClienteVO();

        clienteVO.setId( clienteDTO.getId() );
        clienteVO.setNombre( clienteDTO.getNombre() );
        clienteVO.setGenero( clienteDTO.getGenero() );
        clienteVO.setEdad( clienteDTO.getEdad() );
        clienteVO.setIdentificacion( clienteDTO.getIdentificacion() );
        clienteVO.setDireccion( clienteDTO.getDireccion() );
        clienteVO.setTelefono( clienteDTO.getTelefono() );
        clienteVO.setPassword( clienteDTO.getPassword() );
        clienteVO.setEstado( clienteDTO.getEstado() );

        return clienteVO;
    }
}
