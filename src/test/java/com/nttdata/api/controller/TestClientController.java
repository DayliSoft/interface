package com.nttdata.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.api.app.dto.ClienteDTO;
import com.nttdata.api.app.repository.ClienteRepository;
import com.nttdata.api.app.services.controller.ClienteController;
import com.nttdata.api.app.vo.ClienteVO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class TestClientController  {

    @InjectMocks
    private ClienteController controller;

    @Mock
    private ClienteRepository clienteRepository;

    @Autowired
    private MockMvc mockMvc;

    private static final ObjectMapper mapper = new ObjectMapper();


    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testGetClienteById() throws Exception {
        System.out.println("INIT testGetClienteById");
        Long clientId = 5L;
        ClienteDTO clienteDTO = new ClienteDTO();
        when(this.clienteRepository.findById(clientId)).thenReturn(Optional.of(clienteDTO));


        this.mockMvc.perform(get("/api/cliente/find/{id}", clientId).contextPath("/api")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testSaveCliente() throws Exception {
        System.out.println("INIT testSaveCliente");
        ClienteDTO clienteDTO = new ClienteDTO();
        clienteDTO.setNombre("Jose Lema");
        clienteDTO.setGenero("Masculino");
        clienteDTO.setDireccion("Otavalo sn y principal");
        clienteDTO.setTelefono("098254785");
        clienteDTO.setIdentificacion("0706418514");
        clienteDTO.setPassword("mn 1234");
        clienteDTO.setEstado(true);
        when(this.clienteRepository.save(any(ClienteDTO.class))).thenReturn(clienteDTO);

        ClienteVO clienteVO = new ClienteVO();
        clienteVO.setNombre("Jose Lema");
        clienteVO.setGenero("Masculino");
        clienteVO.setDireccion("Otavalo sn y principal");
        clienteVO.setTelefono("098254785");
        clienteVO.setIdentificacion("0706418514");
        clienteVO.setPassword("mn 1234");
        clienteVO.setEstado(true);
        this.mockMvc.perform(put("/api/cliente/save").contextPath("/api")
                        .content(mapper.writeValueAsString(clienteVO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }
}
