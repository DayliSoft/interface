package com.nttdata.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.api.app.dto.CuentaDTO;
import com.nttdata.api.app.dto.MovimientoDTO;
import com.nttdata.api.app.repository.CuentaRepository;
import com.nttdata.api.app.repository.MovimientoRepository;
import com.nttdata.api.app.services.controller.MovimientoController;
import com.nttdata.api.app.vo.CuentaVO;
import com.nttdata.api.app.vo.MovimientoVO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class TestMovimientoController {

    @InjectMocks
    private MovimientoController movimientoController;

    @Mock
    private MovimientoRepository movimientoRepository;

    @Mock
    private CuentaRepository cuentaRepository;

    @Autowired
    private MockMvc mockMvc;

    private static final ObjectMapper mapper = new ObjectMapper();


    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(movimientoController).build();
    }

    @Test
    public void testGetMovimientoById() throws Exception {
        Long movimientoId = 0L;
        MovimientoDTO movimientoDTO = new MovimientoDTO();
        when(this.movimientoRepository.findById(any(Long.class))).thenReturn(Optional.of(movimientoDTO));

        this.mockMvc.perform(get("/api/movimientos/find/{id}", movimientoId).contextPath("/api")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testSaveMovimiento() throws Exception {
        MovimientoDTO movimientoDTO = new MovimientoDTO();
        movimientoDTO.setId(1L);
        movimientoDTO.setTipoMovimiento("Retiro");
        movimientoDTO.setFecha(new Date());
        movimientoDTO.setValor(-200F);
        movimientoDTO.setSaldo(500F);
        CuentaDTO cuenta = new CuentaDTO();
        cuenta.setId(1L);
        cuenta.setNumeroCuenta("478758");
        cuenta.setTipoCuenta("Ahorros");
        cuenta.setSaldoInicial(700F);
        cuenta.setClienteId(1L);
        cuenta.setEstado(true);
        movimientoDTO.setCuenta(cuenta);
        movimientoDTO.setEstado(true);
        when(this.cuentaRepository.findByNumberAndType(cuenta.getNumeroCuenta(), cuenta.getTipoCuenta())).thenReturn(cuenta);
        when(this.movimientoRepository.findMovimientosByCuentaId(any(Long.class))).thenReturn(Arrays.asList(movimientoDTO));
        when(this.movimientoRepository.save(any(MovimientoDTO.class))).thenReturn(movimientoDTO);

        MovimientoVO movimientoVO = new MovimientoVO();
        movimientoVO.setTipoMovimiento("Retiro");
        movimientoVO.setValor(-200F);
        movimientoVO.setSaldo(500F);
        CuentaVO cuentaVO = new CuentaVO();
        cuentaVO.setNumeroCuenta("478758");
        cuentaVO.setTipoCuenta("Ahorros");
        movimientoVO.setCuenta(cuentaVO);
        movimientoVO.setEstado(true);
        movimientoVO.setFechaTransaccion("10-07-2022");
        this.mockMvc.perform(put("/api/movimientos/save").contextPath("/api")
                        .content(mapper.writeValueAsString(movimientoVO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());

    }
}
