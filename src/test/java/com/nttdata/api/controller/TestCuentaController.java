package com.nttdata.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.api.app.dto.CuentaDTO;
import com.nttdata.api.app.repository.ClienteRepository;
import com.nttdata.api.app.repository.CuentaRepository;
import com.nttdata.api.app.services.controller.CuentaController;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)

public class TestCuentaController {

    @InjectMocks
    private CuentaController controller;

    @Mock
    private CuentaRepository cuentaRepository;

    @Mock
    private ClienteRepository clienteRepository;

    @Autowired
    private MockMvc mockMvc;

    private static final ObjectMapper mapper = new ObjectMapper();


    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void testGetCuentaById() throws Exception {
        Long cuentaId = 5L;
        CuentaDTO cuentaDTO = new CuentaDTO();
        cuentaDTO.setId(cuentaId);
        cuentaDTO.setNumeroCuenta("478758");
        cuentaDTO.setTipoCuenta("Ahorros");
        cuentaDTO.setSaldoInicial(200F);
        cuentaDTO.setEstado(true);
        cuentaDTO.setClienteId(1L);
        when(this.cuentaRepository.findById(cuentaId)).thenReturn(Optional.of(cuentaDTO));

        this.mockMvc.perform(get("/api/cuenta/find/{id}", cuentaId).contextPath("/api")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());

    }

    @Test
    public void testDeleteCuenta() throws Exception {
        Long clientId = 1L;

        this.mockMvc.perform(delete("/api/cuenta/delete/{id}", clientId).contextPath("/api")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());

        verify(cuentaRepository, times(1)).deleteById(any(Long.class));


    }
}
