package com.nttdata.api.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.nttdata.api.*")
@ComponentScan(basePackages="com.nttdata.api.*")
@EntityScan("com.nttdata.api.*")
@Slf4j
public class InterfaceApplication {

	public static void main(String[] args) {
		log.info("That's correct");
		SpringApplication.run(InterfaceApplication.class, args);
	}

}
