package com.nttdata.api.app.services.controller;

import com.nttdata.api.app.dto.ClienteDTO;
import com.nttdata.api.app.dto.mapper.ClienteMapper;
import com.nttdata.api.app.repository.ClienteRepository;
import com.nttdata.api.app.vo.ClienteVO;
import com.nttdata.api.app.vo.ResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("cliente")
@Slf4j
public class ClienteController {

    private static final String PROCESS_SUCCESS = "Proceso realizado";
    private static final String PROCESS_ERROR = "Proceso con error";

    private static final String ERROR = "ERROR";

    private static final String SUCCESS = "SUCCESS";

    @Autowired
    private ClienteRepository clienteRepository;


    /**
     * Buscar cliente por id
     * @param clientId id del cliente
     * @return cliente encontrado
     */
    @GetMapping("/find/{clientId}")
    public ResponseEntity<ResponseVO> find(@PathVariable Long clientId) {
        ResponseVO response = new ResponseVO();
        HttpHeaders headers = new HttpHeaders();

        String messageHeader = PROCESS_ERROR;
        String mensajeBody = "Ha ocurrido un error al buscar el cliente.";
        String type = ERROR;
        Object respuestaBody = null;

        try {
            ClienteDTO clientFind = clienteRepository.findById(clientId).orElse(null);
            if (Objects.nonNull(clientFind)) {
                respuestaBody = ClienteMapper.MAPPER.toVo(clientFind);
                messageHeader = PROCESS_SUCCESS;
                mensajeBody = "Cliente encontrado";
                type = SUCCESS;
            } else{
                mensajeBody = "Cliente no encontrado";
            }
        }catch (Exception e){
            log.error("Error {}", e);
        }
        headers.set("message", messageHeader);
        headers.set("type", type);

        response.setMensaje(mensajeBody);
        response.setRespuesta(respuestaBody);
        response.setType(type);
        return new ResponseEntity<ResponseVO>(response, headers, HttpStatus.OK);
    }


    /**
     * Guardar cliente
     * @param cliente model clienteVO
     * @param bindingResult Capturar errores
     * @return cliente guardado
     */
    @PutMapping("/save")
    public ResponseEntity<ResponseVO> save(@RequestBody @Validated ClienteVO cliente, BindingResult bindingResult) {
        HttpHeaders headers = new HttpHeaders();
        ResponseVO response = new ResponseVO();

        Object respuesta = null;
        String messageHeader = PROCESS_ERROR;
        String mensajeBody = "Ha ocurrido un error al crear el cliente";
        String type = ERROR;

        try {
            if (bindingResult.hasErrors()) {
                mensajeBody = "El cliente no pudo ser creado";
                respuesta = this.obtenerErroresCliente(bindingResult.getAllErrors());
            } else {
                ClienteDTO clientSave = clienteRepository.save(ClienteMapper.MAPPER.toDto(cliente));
                respuesta = ClienteMapper.MAPPER.toVo(clientSave);
                messageHeader = PROCESS_SUCCESS;
                mensajeBody = "El cliente ha sido creado";
                type = SUCCESS;
            }
        } catch (Exception e){
            log.error("Error {}", e);
            mensajeBody = "Ha ocurrido un error al crear el cliente" +e.toString();
        }
        headers.set("message", messageHeader);
        headers.set("type", type);

        response.setMensaje(mensajeBody);
        response.setRespuesta(respuesta);
        response.setType(type);
        return new ResponseEntity<ResponseVO>(response, headers, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<ResponseVO> update(@RequestBody @Validated ClienteVO cliente, BindingResult bindingResult) {
        HttpHeaders headers = new HttpHeaders();
        ResponseVO response = new ResponseVO();

        Object respuesta = null;
        String messageHeader = PROCESS_ERROR;
        String mensajeBody = "Ha ocurrido un error al actualizar el cliente";
        String type = ERROR;

        try {
            if (bindingResult.hasErrors()) {
                mensajeBody = "El cliente no pudo ser actualizado";
                respuesta = this.obtenerErroresCliente(bindingResult.getAllErrors());
            } else {
                ClienteDTO clientFind = clienteRepository.findById(cliente.getId()).orElse(null);
                if (Objects.nonNull(clientFind)) {
                    ClienteDTO clientSave = clienteRepository.save(ClienteMapper.MAPPER.toDto(cliente));
                    respuesta = ClienteMapper.MAPPER.toVo(clientSave);
                    messageHeader = PROCESS_SUCCESS;
                    mensajeBody = "El cliente ha sido actualizado";
                    type = SUCCESS;
                } else{
                    mensajeBody = "Cliente no encontrado";
                }

            }
        } catch (Exception e){
            log.error("Error {}", e);
            mensajeBody = "Ha ocurrido un error al crear el cliente" +e.toString();
        }
        headers.set("message", messageHeader);
        headers.set("type", type);

        response.setMensaje(mensajeBody);
        response.setRespuesta(respuesta);
        response.setType(type);
        return new ResponseEntity<ResponseVO>(response, headers, HttpStatus.OK);
    }

    /**
     * Eliminar cliente
     * @param id id del cliente
     * @return respuesta al eliminar
     */
    @DeleteMapping("/delete/{id}")
    ResponseEntity<ResponseVO> delete(@PathVariable Long id) {
        ResponseVO response = new ResponseVO();
        HttpHeaders headers = new HttpHeaders();

        String messageHeader = PROCESS_ERROR;
        String mensajeBody = "Ha ocurrido un error al eliminar el cliente";
        String type = ERROR;


        try {
            clienteRepository.deleteById(id);
            messageHeader = PROCESS_SUCCESS;
            mensajeBody = "Cliente eliminado";
            type = SUCCESS;
        }catch (EmptyResultDataAccessException e){
            log.error("Error {}", e);
            mensajeBody = "No se ha eliminado ningun cliente";
        } catch (DataIntegrityViolationException e){
            log.error("Error {}", e);
            mensajeBody = "El cliente esta siendo ocupado por otro proceso, no se puede eliminar";
        } catch (Exception e){
            log.error("Error {0}", e);
        }
        headers.set("message", messageHeader);
        headers.set("type", type);

        response.setMensaje(mensajeBody);
        response.setType(type);
        return new ResponseEntity<ResponseVO>(response, headers, HttpStatus.OK);

    }


    /**
     * Obtener errores de los clientes
     * @param listError Lista de errores
     * @return Mapa de errores
     */
    private Map<String, String> obtenerErroresCliente(final List<ObjectError> listError){
        Map<String, String> errors = new HashMap<>();
        listError.forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        log.error("Cantidad de errores al crear el cliente {}", listError.size());
        return errors;
    }
}
