package com.nttdata.api.app.services.controller;

import com.nttdata.api.app.dto.CuentaDTO;
import com.nttdata.api.app.dto.MovimientoDTO;
import com.nttdata.api.app.dto.mapper.CuentaMapper;
import com.nttdata.api.app.dto.mapper.MovimientoMapper;
import com.nttdata.api.app.repository.CuentaRepository;
import com.nttdata.api.app.repository.MovimientoRepository;
import com.nttdata.api.app.vo.CuentaVO;
import com.nttdata.api.app.vo.MovimientoVO;
import com.nttdata.api.app.vo.ReportResponseVO;
import com.nttdata.api.app.vo.ResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("movimientos")
@Slf4j
public class MovimientoController {

    private static final String PROCESS_SUCCESS = "Proceso realizado";
    private static final String PROCESS_ERROR = "Proceso con error";

    private static final String ERROR = "ERROR";

    private static final String SUCCESS = "SUCCESS";

    private static final SimpleDateFormat formatterDDMMYYYY = new SimpleDateFormat("dd-MM-yyyy");

    private static final SimpleDateFormat formatterDDMMYYYYHHMMSS = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    @Autowired
    private MovimientoRepository movimientoRepository;

    @Autowired
    private CuentaRepository cuentaRepository;

    /**
     * Buscar movimiento
     * @param movimientoId id del movimiento
     * @return movimiento encontrado
     */
    @GetMapping("/find/{movimientoId}")
    ResponseEntity<ResponseVO> find(@PathVariable Long movimientoId) {
        ResponseVO response = new ResponseVO();
        HttpHeaders headers = new HttpHeaders();

        String messageHeader = PROCESS_ERROR;
        String mensajeBody = "Ha ocurrido un error al buscar el movimiento";
        Object respuestaBody = null;
        String type = ERROR;
        try {
            MovimientoDTO movimientoDTO = movimientoRepository.findById(movimientoId).orElse(null);
            if (Objects.nonNull(movimientoDTO)) {
                respuestaBody = MovimientoMapper.MAPPER.toVo(movimientoDTO);
                messageHeader = PROCESS_SUCCESS;
                mensajeBody = "Movimiento encontrado";
                type = SUCCESS;
            } else {
                mensajeBody = "Movimiento no encontrado";
            }
        } catch (Exception e) {
            log.error("Error {}", e);
        }
        headers.set("message", messageHeader);
        headers.set("type", type);

        response.setMensaje(mensajeBody);
        response.setRespuesta(respuestaBody);
        response.setType(type);
        return new ResponseEntity<ResponseVO>(response, headers, HttpStatus.OK);
    }

    /**
     * Guardar movimiento
     * @param movimiento model movimiento
     * @param bindingResult captura errores
     * @return movimiento guardado
     */
    @PutMapping("/save")
    ResponseEntity<ResponseVO> save(@RequestBody @Validated MovimientoVO movimiento, BindingResult bindingResult) {
        HttpHeaders headers = new HttpHeaders();
        ResponseVO response = new ResponseVO();

        Object respuesta = null;


        String messageHeader = PROCESS_ERROR;
        String mensajeBody = "Ha ocurrido un error al crear el movimiento";
        String type = ERROR;

        try {
            if (bindingResult.hasErrors()) {
                mensajeBody = "El movimiento no pudo ser creado";
                respuesta = this.obtenerErroresMovimiento(bindingResult.getAllErrors());
            } else {
                CuentaDTO cuentaFind = cuentaRepository.findByNumberAndType(
                        movimiento.getCuenta().getNumeroCuenta(),
                        movimiento.getCuenta().getTipoCuenta());
                if(Objects.nonNull(cuentaFind)){
                    MovimientoDTO movimientoDto = MovimientoMapper.MAPPER.toDto(movimiento);
                    movimientoDto.setCuentaId(cuentaFind.getId());
                    movimientoDto.setCuenta(cuentaFind);
                    List<MovimientoDTO> lastMovimiento =  movimientoRepository.findMovimientosByCuentaId(cuentaFind.getId());
                    Float balance = cuentaFind.getSaldoInicial();
                    if(Objects.nonNull(lastMovimiento) && !lastMovimiento.isEmpty()){
                        balance = lastMovimiento.iterator().next().getSaldo();
                    }
                    Boolean passValidationValues = true;
                    if(movimiento.getValor() >= 0){
                        balance = balance + movimiento.getValor();
                        movimientoDto.setTipoMovimiento("Deposito");
                    } else {
                        Float positiveValor = Math.abs(movimiento.getValor());
                        if(balance <  positiveValor){
                            passValidationValues = false;
                            mensajeBody = "Saldo no disponible";
                        }
                        balance = balance - positiveValor;
                        movimientoDto.setTipoMovimiento("Retiro");
                    }
                    if(passValidationValues) {
                        Date fecha =  formatterDDMMYYYY.parse(movimiento.getFechaTransaccion());
                        movimientoDto.setFecha(fecha);
                        movimientoDto.setSaldo(balance);
                        MovimientoDTO movimientoSave = movimientoRepository.save(movimientoDto);
                        respuesta = MovimientoMapper.MAPPER.toVo(movimientoSave);
                        messageHeader = PROCESS_SUCCESS;
                        mensajeBody = "El movimiento ha sido creado";
                        type = SUCCESS;
                    }
                } else{
                    mensajeBody = "Cuenta no encontrada";
                }
            }
        } catch (Exception e){
            log.error("Error {}", e);
        }
        headers.set("message", messageHeader);
        headers.set("type", type);

        response.setMensaje(mensajeBody);
        response.setRespuesta(respuesta);
        response.setType(type);
        return new ResponseEntity<ResponseVO>(response, headers, HttpStatus.OK);

    }

    @PutMapping("/update")
    ResponseEntity<ResponseVO> update(@RequestBody @Validated MovimientoVO movimiento, BindingResult bindingResult) {
        HttpHeaders headers = new HttpHeaders();
        ResponseVO response = new ResponseVO();

        String messageHeader = PROCESS_ERROR;
        String messageBody = "Ha ocurrido un error al actualizar el movimiento";
        Object respuesta = null;
        String type = ERROR;

        try {
            if (bindingResult.hasErrors()) {
                messageBody = "El movimiento cuenta no pudo ser actualizado";
                respuesta = this.obtenerErroresMovimiento(bindingResult.getAllErrors());
            } else {
                MovimientoDTO movimientoFind = movimientoRepository.findById(movimiento.getId()).orElse(null);
                if (Objects.nonNull(movimientoFind)) {
                    MovimientoDTO movimientoDto = MovimientoMapper.MAPPER.toDto(movimiento);
                    movimientoDto.setCuentaId(movimientoFind.getCuentaId());
                    movimientoDto.setCuenta(movimientoFind.getCuenta());
                    movimientoDto.setSaldo(movimientoFind.getSaldo());
                    Date fecha =  formatterDDMMYYYY.parse(movimiento.getFechaTransaccion());
                    movimientoDto.setFecha(fecha);
                    MovimientoDTO movimientoSave = movimientoRepository.save(movimientoDto);
                    respuesta = MovimientoMapper.MAPPER.toVo(movimientoSave);
                    messageBody = "El movimiento ha sido actualizado";
                    type = SUCCESS;
                } else{
                    messageBody = "Movimiento no encontrado";
                }
            }
        } catch (Exception e){
            log.error("Error {}", e);
        }
        headers.set("message", messageHeader);
        headers.set("type", type);

        response.setMensaje(messageBody);
        response.setRespuesta(respuesta);
        response.setType(type);
        return new ResponseEntity<ResponseVO>(response, headers, HttpStatus.OK);
    }

    /**
     * Eliminar un movimimiento
     * @param id id del movimiento
     * @return movimiento eliminado
     */
    @DeleteMapping("/delete/{id}")
    ResponseEntity<ResponseVO> delete(@PathVariable Long id) {
        ResponseVO response = new ResponseVO();
        HttpHeaders headers = new HttpHeaders();

        String messageHeader = PROCESS_ERROR;
        String mensajeBody = "Ha ocurrido un error al eliminar el movimiento";
        String type = ERROR;

        try {
            movimientoRepository.deleteById(id);
            messageHeader = PROCESS_SUCCESS;
            mensajeBody = "Movimiento eliminado";
            type = SUCCESS;
        }catch (EmptyResultDataAccessException e){
            log.error("Error {}", e);
            mensajeBody = "No se ha eliminado ningun movimiento";
        } catch (Exception e){
            log.error("Error {0}", e);
        }
        headers.set("message", messageHeader);
        headers.set("type", type);

        response.setMensaje(mensajeBody);
        response.setType(type);
        return new ResponseEntity<ResponseVO>(response, headers, HttpStatus.OK);
    }

    /**
     * Generar reportes por rango de fechas y id de cliente
     * @param clienteId id del cliente
     * @param initDate fecha de inicio
     * @param endDate fecha final
     * @return
     */
    @GetMapping("/report/{clienteId}/{initDate}/{endDate}")
    ResponseEntity<ResponseVO> report(@PathVariable Long clienteId, @PathVariable String initDate, @PathVariable String endDate) {
        ResponseVO response = new ResponseVO();
        HttpHeaders headers = new HttpHeaders();

        String messageHeader = PROCESS_ERROR;
        String mensajeBody = "Ha ocurrido un error al consultar los movimientos";
        String type = ERROR;
        Object respuesta = null;


        try {

            Date dateInit = this.obtenerFechaInicio(initDate);
            Date dateEnd = this.obtenerFechaFin(endDate);

            respuesta = this.convertReportResponse(
                    movimientoRepository.reportByDateAndClient(clienteId, dateInit, dateEnd));

            messageHeader = PROCESS_SUCCESS;
            mensajeBody = "Listado";
            type = SUCCESS;
        }catch (Exception e){
            log.error("Error {0}", e);
        }

        headers.set("message", messageHeader);
        headers.set("type", type);

        response.setMensaje(mensajeBody);
        response.setRespuesta(respuesta);
        response.setType(type);
        return new ResponseEntity<ResponseVO>(response, headers, HttpStatus.OK);
    }

    /**
     * Obtener errores al crear el documentos
     * @param listError lista de errores capturados
     * @return mapa de errores
     */
    private Map<String, String> obtenerErroresMovimiento(final List<ObjectError> listError){
        Map<String, String> errors = new HashMap<>();
        listError.forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        log.error("Cantidad de errores al crear el movimiento {}", listError.size());
        return errors;
    }

    /**
     * Obtener fecha inicio
     * @param fecha fecha
     * @return Date
     * @throws ParseException
     */
    private Date obtenerFechaInicio(String fecha) throws ParseException {
        Date dateInit = formatterDDMMYYYY.parse(fecha);
        dateInit.setHours(0);
        dateInit.setMinutes(0);
        dateInit.setSeconds(0);
        return  dateInit;
    }

    /**
     * Obtener fecha fin
     * @param fecha fecha
     * @return Date
     * @throws ParseException
     */
    private Date obtenerFechaFin(String fecha) throws ParseException {
        Date dateEnd = formatterDDMMYYYY.parse(fecha);
        dateEnd.setHours(23);
        dateEnd.setMinutes(59);
        dateEnd.setSeconds(59);
        return  dateEnd;
    }

    /**
     * Convertir lista de movimientos
     * @param movimientos lista de movimientos
     * @return report
     */
    private List<ReportResponseVO> convertReportResponse(List<MovimientoDTO> movimientos){
        List<ReportResponseVO> report = new ArrayList<>();
        if(!movimientos.isEmpty()){
            for (MovimientoDTO movimientoDTO: movimientos) {
                report.add(ReportResponseVO.builder()
                        .fecha(movimientoDTO.getFecha())
                        .cliente(movimientoDTO.getCuenta().getCliente().getNombre())
                        .numeroCuenta(movimientoDTO.getCuenta().getNumeroCuenta())
                        .tipo(movimientoDTO.getCuenta().getTipoCuenta())
                        .saldoInicial(movimientoDTO.getCuenta().getSaldoInicial())
                        .estado(movimientoDTO.getEstado())
                        .movimiento(movimientoDTO.getValor())
                        .saldoDisponible(movimientoDTO.getSaldo())
                        .build());
            }
        }
        return report;
    }


}
