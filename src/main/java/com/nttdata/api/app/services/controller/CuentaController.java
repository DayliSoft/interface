package com.nttdata.api.app.services.controller;

import com.nttdata.api.app.dto.ClienteDTO;
import com.nttdata.api.app.dto.CuentaDTO;
import com.nttdata.api.app.dto.mapper.CuentaMapper;
import com.nttdata.api.app.repository.ClienteRepository;
import com.nttdata.api.app.repository.CuentaRepository;
import com.nttdata.api.app.vo.CuentaVO;
import com.nttdata.api.app.vo.ResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@RestController
@RequestMapping("cuenta")
@Slf4j
public class CuentaController {


    private static final String PROCESS_SUCCESS = "Proceso realizado";
    private static final String PROCESS_ERROR = "Proceso con error";

    private static final String ERROR = "ERROR";

    private static final String SUCCESS = "SUCCESS";

    @Autowired
    private CuentaRepository cuentaRepository;

    @Autowired
    private ClienteRepository clienteRepository;


    /**
     * Buscar cuenta
     * @param cuentaId id de la cuenta
     * @return cuenta encontrada
     */
    @GetMapping("/find/{cuentaId}")
    ResponseEntity<ResponseVO> find(@PathVariable Long cuentaId) {

        ResponseVO response = new ResponseVO();
        HttpHeaders headers = new HttpHeaders();

        String messageHeader = PROCESS_ERROR;
        String mensajeBody = "Ha ocurrido un error al buscar la cuenta";
        Object respuestaBody = null;
        String type = ERROR;
        try {
            CuentaDTO cuentaFind = cuentaRepository.findById(cuentaId).orElse(null);
            if (Objects.nonNull(cuentaFind)) {
                respuestaBody = CuentaMapper.MAPPER.toVo(cuentaFind);
                messageHeader = PROCESS_SUCCESS;
                mensajeBody = "Cuenta encontrada";
                type = SUCCESS;
            } else{
                mensajeBody = "Cuenta no encontrada";
            }
        }catch (Exception e){
            log.error("Error {}", e);
        }

        headers.set("message", messageHeader);
        headers.set("type", type);

        response.setMensaje(mensajeBody);
        response.setRespuesta(respuestaBody);
        response.setType(type);
        return new ResponseEntity<ResponseVO>(response, headers, HttpStatus.OK);
    }

    /**
     * Guardar cuenta
     * @param cuenta model de la cuenta
     * @param bindingResult captura errores
     * @return Cuenta guardada
     */
    @PutMapping("/save")
    ResponseEntity<ResponseVO> save(@RequestBody @Validated CuentaVO cuenta, BindingResult bindingResult) {
        HttpHeaders headers = new HttpHeaders();
        ResponseVO response = new ResponseVO();

        String messageHeader = PROCESS_ERROR;
        String messageBody = "Ha ocurrido un error al crear la cuenta";
        Object respuesta = null;
        String type = ERROR;

        try {
            if (bindingResult.hasErrors()) {
                messageBody = "La cuenta no pudo ser creada";
                respuesta = this.obtenerErroresCuenta(bindingResult.getAllErrors());
            } else {
                ClienteDTO clientFind = clienteRepository.findById(cuenta.getClienteId()).orElse(null);
                if(Objects.nonNull(clientFind)){
                    CuentaDTO cuentaFind = cuentaRepository.findByNumberAndType(
                            cuenta.getNumeroCuenta(),
                            cuenta.getTipoCuenta());
                    if(Objects.isNull(cuentaFind)){
                        CuentaDTO cuentaSave = cuentaRepository.save(CuentaMapper.MAPPER.toDto(cuenta));
                        respuesta = CuentaMapper.MAPPER.toVo(cuentaSave);
                        messageBody = "La cuenta ha sido creada";
                        type = SUCCESS;
                    } else {
                        messageBody = "No se puedo crear el registro, la cuenta ya existe";
                    }
                } else{
                    messageBody = "Cliente no encontrado";
                }
            }
        } catch (Exception e){
            log.error("Error {}", e);
        }
        headers.set("message", messageHeader);
        headers.set("type", type);

        response.setMensaje(messageBody);
        response.setRespuesta(respuesta);
        response.setType(type);
        return new ResponseEntity<ResponseVO>(response, headers, HttpStatus.OK);
    }

    @PutMapping("/update")
    ResponseEntity<ResponseVO> update(@RequestBody @Validated CuentaVO cuenta, BindingResult bindingResult) {
        HttpHeaders headers = new HttpHeaders();
        ResponseVO response = new ResponseVO();

        String messageHeader = PROCESS_ERROR;
        String messageBody = "Ha ocurrido un error al actualizar la cuenta";
        Object respuesta = null;
        String type = ERROR;

        try {
            if (bindingResult.hasErrors()) {
                messageBody = "La cuenta no pudo ser actualizada";
                respuesta = this.obtenerErroresCuenta(bindingResult.getAllErrors());
            } else {
                CuentaDTO cuentaFindById = cuentaRepository.findById(cuenta.getId()).orElse(null);
                if (Objects.nonNull(cuentaFindById)) {
                    CuentaDTO cuentaSave = cuentaRepository.save(CuentaMapper.MAPPER.toDto(cuenta));
                    respuesta = CuentaMapper.MAPPER.toVo(cuentaSave);
                    messageBody = "La cuenta ha sido actualizada";
                    type = SUCCESS;
                } else{
                    messageBody = "Cuenta no encontrado";
                }
            }
        } catch (Exception e){
            log.error("Error {}", e);
        }
        headers.set("message", messageHeader);
        headers.set("type", type);

        response.setMensaje(messageBody);
        response.setRespuesta(respuesta);
        response.setType(type);
        return new ResponseEntity<ResponseVO>(response, headers, HttpStatus.OK);
    }

    /**
     * Eliminar cuenta
     * @param id id de la cuenta a eliminar
     * @return respuesta al eliminar
     */
    @DeleteMapping("/delete/{id}")
    ResponseEntity<ResponseVO> delete(@PathVariable Long id) {
        ResponseVO response = new ResponseVO();
        HttpHeaders headers = new HttpHeaders();

        String messageHeader = PROCESS_ERROR;
        String messageBody = "Ha ocurrido un error al eliminar la cuenta";
        String type = ERROR;
        try {
            cuentaRepository.deleteById(id);
            messageHeader = PROCESS_SUCCESS;
            messageBody = "Cuenta eliminada";
            type = SUCCESS;
        }catch (EmptyResultDataAccessException e){
            log.error("Error {}", e);
            messageBody = "No se ha eliminado ninguna cuenta";
        } catch (DataIntegrityViolationException e){
            log.error("Error {}", e);
            messageBody = "La cuenta esta siendo ocupado por otro proceso, no se puede eliminar";
        } catch (Exception e){
            log.error("Error {}", e);
        }
        headers.set("message", messageHeader);
        headers.set("type", type);

        response.setMensaje(messageBody);
        response.setType(type);
        return new ResponseEntity<ResponseVO>(response, headers, HttpStatus.OK);
    }

    /**
     * Obtener errores al guardar una cuenta
     * @param listError lista de errores
     * @return mapa de errores
     */
    private Map<String, String> obtenerErroresCuenta(final List<ObjectError> listError){
        Map<String, String> errors = new HashMap<>();
        listError.forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        log.error("Cantidad de errores al crear la cuenta {}", listError.size());
        return errors;
    }


}
