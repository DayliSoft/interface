package com.nttdata.api.app.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="t_cuenta")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class CuentaDTO {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;

    @Column(name = "NUMEROCUENTA", nullable = false)
    private String numeroCuenta;

    @Column(name = "TIPOCUENTA", nullable = false)
    private String tipoCuenta;

    @Column(name = "SALDOINICIAL", nullable = false)
    private Float saldoInicial;

    @Column(name = "ESTADO", nullable = false)
    private Boolean estado;



    //@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(targetEntity = ClienteDTO.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "CLIENTEID",  referencedColumnName="ID", insertable=false, updatable=false, nullable = false)
    private ClienteDTO cliente;


    @Column(name="CLIENTEID", nullable = false)
    private Long clienteId;

}
