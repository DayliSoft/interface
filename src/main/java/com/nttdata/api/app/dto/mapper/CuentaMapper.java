package com.nttdata.api.app.dto.mapper;

import com.nttdata.api.app.dto.CuentaDTO;
import com.nttdata.api.app.vo.CuentaVO;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CuentaMapper {

    CuentaMapper MAPPER = Mappers.getMapper( CuentaMapper.class );

    @InheritInverseConfiguration
    CuentaDTO toDto(CuentaVO cuentaVO);

    CuentaVO toVo(CuentaDTO cuentaDTO);
}
