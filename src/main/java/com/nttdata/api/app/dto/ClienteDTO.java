package com.nttdata.api.app.dto;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

@Entity
@Table(name="t_cliente")
@Getter
@Setter
@PrimaryKeyJoinColumn(name = "ID")
public class ClienteDTO extends PersonaDTO  {

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "ESTADO", nullable = false)
    private Boolean estado;


}
