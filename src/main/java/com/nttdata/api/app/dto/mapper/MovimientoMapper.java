package com.nttdata.api.app.dto.mapper;

import com.nttdata.api.app.dto.MovimientoDTO;
import com.nttdata.api.app.vo.MovimientoVO;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MovimientoMapper {

    MovimientoMapper MAPPER = Mappers.getMapper( MovimientoMapper.class );

    @InheritInverseConfiguration
    MovimientoDTO toDto(MovimientoVO movimientoVO);


    @Mappings({ @Mapping(target = "fechaTransaccion", ignore = true)})
    MovimientoVO toVo(MovimientoDTO movimientoDTO);
}
