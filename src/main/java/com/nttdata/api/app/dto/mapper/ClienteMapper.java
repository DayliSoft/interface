package com.nttdata.api.app.dto.mapper;

import com.nttdata.api.app.dto.ClienteDTO;
import com.nttdata.api.app.vo.ClienteVO;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ClienteMapper {

    ClienteMapper MAPPER = Mappers.getMapper( ClienteMapper.class );

    @InheritInverseConfiguration
    ClienteDTO toDto(ClienteVO clienteVO);

    ClienteVO toVo(ClienteDTO clienteDTO);
}
