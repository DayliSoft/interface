package com.nttdata.api.app.repository;

import com.nttdata.api.app.dto.CuentaDTO;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Lazy
@Repository
public interface CuentaRepository extends JpaRepository<CuentaDTO, Long> {

    @Query("select u from CuentaDTO u where u.numeroCuenta = :numeroCuenta and u.tipoCuenta = :tipoCuenta")
    CuentaDTO findByNumberAndType(String numeroCuenta, String tipoCuenta);
}
