package com.nttdata.api.app.vo;

import lombok.Data;

import javax.validation.constraints.*;


@Data
public class CuentaVO {

    private Long id;

    @NotNull(message = "El campo numero de cuenta no puede ser nulo")
    @NotBlank(message = "El campo numero de cuenta no puede estar vacio")
    private String numeroCuenta;

    @NotNull(message = "El campo tipo de cuenta no puede ser nulo")
    @NotBlank(message = "El campo tipo de cuenta no puede estar vacio")
    private String tipoCuenta;

    @NotNull(message = "El campo saldo inicial no puede ser nulo")
    @PositiveOrZero(message = "El saldo inicial debe ser positivo")
    private Float saldoInicial;

    @NotNull(message = "El campo estado no puede ser nulo")
    private Boolean estado;

    @NotNull(message = "El id del cliente no puede ser nulo")
    private Long clienteId;

    private ClienteVO cliente;


}
