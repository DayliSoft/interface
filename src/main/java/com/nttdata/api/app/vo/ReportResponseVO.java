package com.nttdata.api.app.vo;

import lombok.Builder;
import lombok.Data;

import java.util.Date;


@Data
@Builder
public class ReportResponseVO {

    private Date fecha;

    private String cliente;

    private String numeroCuenta;

    private String tipo;

    private Float saldoInicial;

    private Boolean estado;

    private Float movimiento;

    private Float saldoDisponible;

}
